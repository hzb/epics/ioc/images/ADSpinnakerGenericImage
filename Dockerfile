FROM registry.hzdr.de/hzb/epics/base/ubuntu_22_04:1.0.0 AS base

RUN mkdir -p /opt/epics/support

RUN apt-get update
# RUN apt install -y libtiff5 libjpeg-turbo8 libxml2 libxml2-dev libzstd1 libaravis-0.8-0 libaravis-dev
RUN apt-get install -y libtiff5\
    libjpeg-turbo8 libzstd1\
    libaravis-0.8-0 libaravis-dev\
    libx11-dev libxcomposite-dev\
    libboost1.74-dev libboost-test-dev\
    libboost-system-dev

RUN apt-get install -y libopencv-dev
RUN apt-get install -y libopencv-contrib-dev

## Install required EPICS modules and areaDetector

# configure environment
COPY configure/epics/RELEASE.local ${SUPPORT}/RELEASE.local

# install autosave 
RUN git clone --depth 1 --recursive --branch R5-10-2 https://github.com/epics-modules/autosave.git ${SUPPORT}/autosave
RUN make -C ${SUPPORT}/autosave -j $(nproc)

# install seq
RUN git clone --depth 1 --recursive --branch vendor_2_2_8 https://github.com/ISISComputingGroup/EPICS-seq.git ${SUPPORT}/seq
RUN make -C ${SUPPORT}/seq -j $(nproc)

# install sscan
RUN git clone --depth 1 --recursive --branch R2-11-5 https://github.com/epics-modules/sscan.git ${SUPPORT}/sscan
RUN make -C ${SUPPORT}/sscan -j $(nproc)

# install calc
RUN git clone --depth 1 --recursive --branch R3-7-4 https://github.com/epics-modules/calc.git ${SUPPORT}/calc
RUN make -C ${SUPPORT}/calc -j $(nproc)

# install asyn
RUN apt install -y libntirpc-dev libtirpc3
RUN git clone --depth 1 --recursive --branch R4-44-2 https://github.com/epics-modules/asyn.git ${SUPPORT}/asyn
RUN sed -i '/TIRPC/s/^#//g' ${SUPPORT}/asyn/configure/CONFIG_SITE
RUN make -C ${SUPPORT}/asyn -j $(nproc)

# install busy
RUN git clone --depth 1 --recursive --branch R1-7-4 https://github.com/epics-modules/busy.git ${SUPPORT}/busy
RUN make -C ${SUPPORT}/busy -j $(nproc)

# install iocStats
RUN git clone --depth 1 --recursive --branch 3.2.0 https://github.com/epics-modules/iocStats.git ${SUPPORT}/iocStats
RUN make -C ${SUPPORT}/iocStats -j $(nproc)

## AreaDetector setup
# clone areaDetector main repo
RUN git clone --branch R3-12-1 https://github.com/areaDetector/areaDetector.git ${SUPPORT}/areaDetector
RUN git clone --branch R3-12-1 https://github.com/areaDetector/ADCore.git ${SUPPORT}/areaDetector/ADCore
RUN git clone --branch R1-10 https://github.com/areaDetector/ADSupport.git ${SUPPORT}/areaDetector/ADSupport
RUN git clone --branch R1-9 https://github.com/areaDetector/ADGenICam.git ${SUPPORT}/areaDetector/ADGenICam
RUN git clone --branch R3-5 https://github.com/areaDetector/ADSpinnaker.git ${SUPPORT}/areaDetector/ADSpinnaker


# configure areaDetector
RUN cd ${SUPPORT}/areaDetector/configure &&\
    cp EXAMPLE_RELEASE.local RELEASE.local &&\
    cp EXAMPLE_RELEASE_LIBS.local RELEASE_LIBS.local &&\
    cp EXAMPLE_CONFIG_SITE.local CONFIG_SITE.local &&\
    cp EXAMPLE_CONFIG_SITE.local.linux-x86_64 CONFIG_SITE.local.linux-x86_64

# configure areaDetector environment
COPY configure/areaDetector/RELEASE_PRODS.local ${SUPPORT}/areaDetector/configure/RELEASE_PRODS.local
COPY configure/areaDetector/RELEASE_LIBS.local ${SUPPORT}/areaDetector/configure/RELEASE_LIBS.local

# include ADGENICAM to RELEASE
RUN sed -i '/ADGENICAM/s/^#//g' ${SUPPORT}/areaDetector/configure/RELEASE.local

# include ADSPINNAKER to RELEASE
RUN sed -i '/ADSPINNAKER/s/^#//g' ${SUPPORT}/areaDetector/configure/RELEASE.local

# add ArUco Plugin
RUN git clone --branch main https://github.com/whs92/adpluginarucounwarp.git ${SUPPORT}/areaDetector/ADPluginArucoUnwarp

# Append required changes to commonDriverMakefile, get this from the local dir. 
COPY configure/commonDriverMakefile.patch ${SUPPORT}/areaDetector/commonDriverMakefile.patch
RUN cat ${SUPPORT}/areaDetector/commonDriverMakefile.patch >> ${SUPPORT}/areaDetector/ADCore/ADApp/commonDriverMakefile

# modify configure/CONFIG_SITE.local
RUN sed -i '/WITH_OPENCV/s/^#//g' ${SUPPORT}/areaDetector/configure/CONFIG_SITE.local
RUN sed -i '/WITH_OPENCV/s/NO/YES/' ${SUPPORT}/areaDetector/configure/CONFIG_SITE.local
RUN sed -i '/OPENCV_EXTERNAL/s/NO/YES/' ${SUPPORT}/areaDetector/configure/CONFIG_SITE.local
RUN echo  "OPENCV_INCLUDE = -I/usr/include/opencv4/opencv -I/usr/include/opencv4" >> ${SUPPORT}/areaDetector/configure/CONFIG_SITE.local
RUN echo "OPENCV = /usr" >> ${SUPPORT}/areaDetector/configure/CONFIG_SITE.local

# modify configure/CONFIG_SITE.local.linux-x86_64
RUN sed -i '/WITH_OPENCV/s/^#//g' ${SUPPORT}/areaDetector/configure/CONFIG_SITE.local.linux-x86_64
RUN sed -i '/WITH_OPENCV/s/NO/YES/' ${SUPPORT}/areaDetector/configure/CONFIG_SITE.local.linux-x86_64
RUN echo  "OPENCV_INCLUDE = -I/usr/include/opencv4/opencv -I/usr/include/opencv4" >> ${SUPPORT}/areaDetector/configure/CONFIG_SITE.local.linux-x86_64
RUN echo "OPENCV = /usr" >> ${SUPPORT}/areaDetector/configure/CONFIG_SITE.local.linux-x86_64

# install areaDetector
RUN make -C ${SUPPORT}/areaDetector/ADSupport -j $(nproc)
RUN make -C ${SUPPORT}/areaDetector/ADCore -j $(nproc)
RUN make -C ${SUPPORT}/areaDetector/ADPluginArucoUnwarp -j $(nproc)
RUN make -C ${SUPPORT}/areaDetector/ADGenICam -j $(nproc)
RUN make -C ${SUPPORT}/areaDetector/ADSpinnaker -j $(nproc)


# set up envPaths and startup file
RUN cd ${SUPPORT}/areaDetector/ADSpinnaker/iocs/spinnakerIOC/iocBoot/iocSpinnaker &&\
    cp envPaths envPaths.linux

# install the spinnaker libraries to the path
COPY spinnaker.conf /etc/ld.so.conf.d/spinnaker.conf
RUN ldconfig
